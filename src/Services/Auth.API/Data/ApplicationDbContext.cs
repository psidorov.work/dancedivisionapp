using Microsoft.EntityFrameworkCore;
using Auth.API.Models;

namespace Auth.API.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {
        }
    public DbSet<Account> Accounts => Set<Account>();

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Account>()
            .ToTable("accounts");
        
        builder.Entity<Account>()
            .HasKey(k => k.Id);

        builder.Entity<Account>()
            .HasData(new Account {Id = 1, Username = "pasha", Password = "secret", IsActive = false});
    }
}