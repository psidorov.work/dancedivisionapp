namespace Auth.API.Models;

public class Account 
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public bool IsActive { get; set; }

    public Account()
    {
        Username = string.Empty;
        Password = string.Empty;
    }
}