using System.ComponentModel.DataAnnotations;

namespace Auth.API.Models;

public class RegisterRequest
{
    [Required]
    public string Username { get; set; } = default!;
    [Required]
    public string Password { get; set; } = default!;
}