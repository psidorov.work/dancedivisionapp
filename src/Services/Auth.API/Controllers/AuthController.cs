using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Auth.API.Data;
using Auth.API.Models;
using Microsoft.AspNetCore.Authorization;

namespace Auth.API.Controllers;

[ApiController]
[Route("[controller]")]
public class AuthController : ControllerBase
{
    private readonly ILogger<AuthController> _logger;
    public IConfiguration _configuration;
    private readonly ApplicationDbContext _context;
    public AuthController(ILogger<AuthController> logger, IConfiguration configuration, ApplicationDbContext context)
    {
        _logger = logger;
        _configuration = configuration;
        _context = context;
    }
    [AllowAnonymous]
    [HttpPost("authenticate")]
    public async Task<IActionResult> Post(string username, string password)
    {
        var account = await _context.Accounts.FirstOrDefaultAsync(f => f.Username == username && f.Password == password);
        if(account != null)
        {
            account.IsActive = true;
            return Ok();
        }
        return BadRequest();
    }
    
    [AllowAnonymous]
    [HttpPost("register")]
    public IActionResult Register(RegisterRequest register)
    {
        if(_context.Accounts.Any(a => a.Username == register.Username))
            return BadRequest(register.Username + " already exist");
        
        var account  = new Account
        {
            Username = register.Username,
            Password = register.Password
        };

        _context.Accounts.Add(account);
        _context.SaveChanges();
        return Ok("register successfull");
    }

    [HttpGet]
    public IActionResult Get()
    {
        return Ok("Dick");
    }
}